def super_counter(start, stop, step):
    print('Super counter from {} to {} with step {}'.format(start, stop, step))
    for i in range(start, stop, step):
        print('Advanced counter: {}'.format(i))
    print('End super counter')
